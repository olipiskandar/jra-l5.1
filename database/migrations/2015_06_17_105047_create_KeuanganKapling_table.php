<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeuanganKaplingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('KeuanganKapling', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_jamaah');
            $table->foreign('id_jamaah')->references('id')->on('Jamaah')->onDelete('CASCADE');
            $table->date('Tanggal');
            $table->integer('JumlahUang');
            $table->integer('DurasiPembayaran');
            $table->string('Keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('KeuanganKapling');
    }
}
