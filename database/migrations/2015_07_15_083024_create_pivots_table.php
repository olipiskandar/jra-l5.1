<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivots', function (Blueprint $table) {
            $table->integer('id_jamaah')->unsigned()->nullable();
            $table->integer('id_kapling')->unsigned()->nullable();
            $table->foreign('id_jamaah')->references('id')->on('jamaah')->onDelete('CASCADE');
            $table->foreign('id_kapling')->references('id')->on('Kapling')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pivots');
    }
}
