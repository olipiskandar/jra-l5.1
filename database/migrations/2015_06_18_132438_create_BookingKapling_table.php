<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingKaplingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BookingKapling', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kapling')->unsigned()->index();
            $table->foreign('id_kapling')->references('id')->on('Kapling')->onDelete('cascade');
            $table->integer('id_jamaah')->unsigned()->index();
            $table->foreign('id_jamaah')->references('id')->on('Jamaah')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('BookingKapling');
    }
}
