<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenanggungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Penanggung', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_jamaah');
            $table->string('NamaLengkap');
            $table->string('Alamat');
            $table->string('Telepon');
            $table->string('Telepon2')->nullable();
            $table->timestamps();

            /*$table->primary(['role_id', 'user_id']);*/

            $table->foreign('id_jamaah')
                  ->references('id')->on('Jamaah')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Penanggung');
    }
}