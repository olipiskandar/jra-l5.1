<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaplingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Kapling', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_unit');
            $table->foreign('id_unit')->references('id')->on('Unit')->onDelete('CASCADE');
            $table->string('KodeKapling')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Kapling');
    }
}