<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJamaahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jamaah', function(Blueprint $table) {
            $table->increments('id');
            $table->string('Status');
            $table->date('TanggalDaftar')->nullable();
            $table->date('TanggalMeninggal')->nullable();
            $table->string('no_jra')->nullable();
            $table->string('NamaJamaah');
            $table->text('AlamatLengkap')->nullable();
            $table->string('Telepon')->nullable();
            $table->integer('Pendaftaran')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jamaah');
    }
}
