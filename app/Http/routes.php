<?php

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::get('home', 'backend\JamaahController@dasboard');
Route::get('/', 'backend\JamaahController@index');
Route::get('dasboard', 'backend\JamaahController@dasboard');
// Jamaah
Route::resource('jamaah', 'backend\JamaahController');
Route::get('hapusjamaah/{id}', array('as' => 'destroyjamaah', 'uses' => 'backend\JamaahController@destroy'));

// Keuangan Anggota
Route::resource('keuanganasuransi', 'backend\KeuanganAsuransiController');
Route::get('hapuskeuanganasuransi/{id}', array('as' => 'destroykeuanganasuransi', 'uses' => 'backend\KeuanganAsuransiController@destroy'));

// Penempatan Makam
Route::resource('penempatan', 'backend\PivotController');
Route::get('hapuspenempatan/{id}', array('as' => 'destroypenempatan', 'uses' => 'backend\PivotController@destroy'));

// Keuangan Kapling
Route::resource('keuangankapling', 'backend\KeuanganKaplingController');
Route::get('hapuskeuangankapling/{id}', array('as' => 'destroykeuangankapling', 'uses' => 'backend\KeuanganKaplingController@destroy'));

// Booking Kapling
Route::resource('bookingkapling', 'backend\BookingKaplingController');
Route::get('hapusbookingkapling/{id}', array('as' => 'destroybookingkapling', 'uses' => 'backend\BookingKaplingController@destroy'));

// Unit Makam
Route::resource('unit', 'backend\UnitController');
Route::get('hapusunit/{id}', array('as' => 'destroyunit', 'uses' => 'backend\UnitController@destroy'));

// Kapling Makam
Route::resource('kapling', 'backend\KaplingController');
Route::get('hapuskapling/{id}', array('as' => 'destroykapling', 'uses' => 'backend\KaplingController@destroy'));

// Penanggung Jawab
Route::resource('penanggungjawab', 'backend\PenanggungController');
Route::get('hapuspenanggungjawab/{id}', array('as' => 'destroypenanggungjawab', 'uses' => 'backend\PenanggungController@destroy'));