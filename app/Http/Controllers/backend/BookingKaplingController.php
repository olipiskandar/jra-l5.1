<?php

namespace App\Http\Controllers\backend;

use DB;
use App\Http\Controllers\Controller;
use App\Model\BookingKapling;
use App\Model\Jamaah;
use App\Model\Kapling;
use App\Model\Unit;
use \Validator, \Redirect, \Request, \Input;


class BookingKaplingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $bookingkapling = BookingKapling::select('bookingkapling.id as id', 'jamaah.namajamaah as NamaJamaah', 'jamaah.id as idjamaah', 'unit.lokasi as Lokasi', 'kapling.kodekapling as KodeKapling')
                            ->join('jamaah', 'bookingkapling.id_jamaah', '=', 'jamaah.id')
                            ->join('kapling', 'bookingkapling.id_kapling', '=', 'kapling.id')
                            ->join('unit', 'kapling.id_unit', '=', 'unit.id')
                            ->get();

        $jamaah = Jamaah::select('jamaah.*')
                    ->leftjoin('bookingkapling', 'bookingkapling.id_jamaah', '=', 'jamaah.id')
                    ->whereNull('bookingkapling.id_jamaah')
                    ->where('jamaah.Status', '=', 'Aktif')
                    ->orderBy('jamaah.namajamaah', 'desc')
                    ->get();
        $kapling = Kapling::select('kapling.*')
                    ->leftjoin('pivots', 'pivots.id_kapling', '=', 'kapling.id')
                    ->leftjoin('bookingkapling', 'bookingkapling.id_kapling', '=', 'kapling.id')
                    ->whereNull('pivots.id_kapling')
                    ->whereNull('bookingkapling.id_kapling')
                    ->get();
        /*dd($kapling);*/

        return view('Makam.Booking.index', compact('bookingkapling', 'jamaah', 'kapling'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $bookingkapling=Request::all();
        BookingKapling::create($bookingkapling);
        /*dd($bookingkapling);*/
        return redirect('bookingkapling');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $booking = BookingKapling::find($id);
        $jamaah = Jamaah::select('jamaah.*')
                    ->leftjoin('bookingkapling', 'bookingkapling.id_jamaah', '=', 'jamaah.id')
                    /*->whereNull('bookingkapling.id_jamaah')*/
                    ->where('jamaah.Status', '=', 'Aktif')
                    ->orderBy('jamaah.namajamaah', 'desc')
                    ->get();
        $kapling = Kapling::select('kapling.*')
                    ->leftjoin('pivots', 'pivots.id_kapling', '=', 'kapling.id')
                    ->leftjoin('bookingkapling', 'bookingkapling.id_kapling', '=', 'kapling.id')
                    ->whereNull('pivots.id_kapling')
                    ->whereNull('bookingkapling.id_kapling')
                    ->get();
        return view('Makam.Booking.edit', compact('booking', 'jamaah', 'kapling'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $bookingupdate = Request::all();
        $booking = BookingKapling::find($id);
        $booking->update($bookingupdate);
        return redirect('bookingkapling');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        BookingKapling::find($id)->delete();
        return redirect('bookingkapling');
    }
}
