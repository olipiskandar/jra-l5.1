<?php

namespace App\Http\Controllers\backend;

use DB;
use App\Http\Controllers\Controller;
use App\Model\Jamaah;
use App\Model\Kapling;
use App\Model\Unit;
use \Validator, \Redirect, \Request, \Input;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $unit = Unit::all();
        return view('Makam.Lokasi.index', compact('unit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $unit=Request::all();
        Unit::create($unit);
        return redirect('unit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $unit   = Unit::find($id);
        $sisa   = Kapling::where('id_unit', '=', $id)->count('id_unit');
        $kapling = Unit::select('kapling.id as idk', 'kapling.kodekapling as KodeKapling', 'jamaah.namajamaah as nama', 'jamaah.id as idjamaah')
                    ->join('kapling', 'kapling.id_unit', '=', 'unit.id')
                    ->leftjoin('pivots', 'pivots.id_kapling', '=', 'kapling.id')
                    ->leftjoin('jamaah', 'pivots.id_jamaah', '=', 'jamaah.id')
                    ->where('kapling.id_unit', '=', $id)
                    ->OrderBy('kapling.id_unit', 'asc')
                    ->get();

        return view('Makam.Lokasi.show', compact('unit', 'sisa', 'kapling'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $unit = Unit::find($id);
        return view('Makam.Lokasi.edit', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $unitupdate = Request::all();
        $unit = Unit::find($id);
        $unit->update($unitupdate);
        return redirect('unit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        Unit::find($id)->delete();
        return redirect('unit');
    }
}