<?php

namespace App\Http\Controllers\backend;

use DB;
use App\Http\Controllers\Controller;
use App\Model\Jamaah;
use App\Model\Kapling;
use App\Model\Unit;
use \Validator, \Redirect, \Request, \Input;

class KaplingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        // return view('Makam.Kapling.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $kapling = Request::all();
        Kapling::create($kapling);
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $unit = unit::all();
        $kapling = Kapling::find($id);
        return view('Makam.Kapling.edit', compact('unit', 'kapling'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $kaplingupdate = Request::all();
        $kapling = Kapling::find($id);
        $kapling->update($kaplingupdate);
        return redirect('unit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        Kapling::find($id)->delete();
        return Redirect::back();
    }
}
