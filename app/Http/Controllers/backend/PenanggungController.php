<?php

namespace App\Http\Controllers\backend;

use DB;
use App\Http\Controllers\Controller;
use App\Model\Jamaah;
use App\Model\Penanggung;
use \Validator, \Redirect, \Request, \Input;

class PenanggungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $penanggung     = Penanggung::select('penanggung.*', 'jamaah.namajamaah as NamaJamaah', 'jamaah.id as idjamaah')
                            ->join('jamaah', 'penanggung.id_jamaah', '=', 'jamaah.id')
                            ->where('jamaah.status', '=', 'Meninggal')
                            ->orderBy('id', 'asc')
                            ->get();
        $jamaah         = Jamaah::where('Status', '=', 'Meninggal')->get();

        return view ('PenanggungJawab.index', compact('penanggung', 'jamaah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $penanggung=Request::all();
        Penanggung::create($penanggung);
        return redirect('penanggungjawab');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $jamaah = Jamaah::where('Status', '=', 'Meninggal')->get();
        $penanggung = Penanggung::find($id);
        return view('penanggungjawab.edit', compact('jamaah', 'penanggung'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $penanggungupdate = Request::all();
        $penanggung = Penanggung::find($id);
        $penanggung->update($penanggungupdate);
        return redirect('penanggungjawab');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        Penanggung::find($id)->delete();
        return redirect('penanggungjawab');
    }
}
