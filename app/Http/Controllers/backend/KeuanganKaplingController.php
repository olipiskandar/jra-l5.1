<?php

namespace App\Http\Controllers\backend;

use Response;
use DB;
use App\Http\Controllers\Controller;
use App\Model\Jamaah;
use App\Model\Pivot;
use App\Model\Unit;
use App\Model\Kapling;
use App\Model\Penanggung;
use App\Model\KeuanganKapling;
use \Validator, \Redirect, \Request, \Input;

class KeuanganKaplingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $keuangan = KeuanganKapling::select('jamaah.id as idj', 
                    'jamaah.namajamaah as NamaJamaah', 
                    'jamaah.no_jra as no_jra',
                    'keuangankapling.*', 
                    'penanggung.namalengkap as NamaLengkap',
                    'kapling.kodekapling as KodeKapling',
                    'unit.lokasi as Lokasi')
                    ->join('jamaah', 'keuangankapling.id_jamaah', '=', 'jamaah.id')
                    ->join('penanggung', 'penanggung.id_jamaah', '=', 'jamaah.id')
                    ->join('pivots', 'pivots.id_jamaah', '=', 'jamaah.id')
                    ->join('kapling', 'pivots.id_kapling', '=','kapling.id')
                    ->join('unit', 'kapling.id_unit', '=', 'unit.id')
                    ->where('jamaah.status', '=', 'Meninggal')
                    ->orderBy('id', 'desc')
                    ->get();
        
        // Tambah Transasksi
        $alljamaah = Penanggung::select('jamaah.id as id', 'jamaah.namajamaah as NamaJamaah', 'penanggung.id as idp', 'penanggung.namalengkap as NamaLengkap')
                    ->join('jamaah', 'penanggung.id_jamaah', '=', 'jamaah.id')
                    ->get();
        return view('Keuangan.Kapling.index', compact('keuangan', 'alljamaah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $keuangan = Request::all();
        $kirim = KeuanganKapling::create($keuangan);
        $insertedId = $kirim->id;
        return redirect('keuangankapling/'.$insertedId);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $keuangankapling = KeuanganKapling::find($id);
        $print = KeuanganKapling::select('kapling.kodekapling as KodeKapling', 'unit.lokasi as Lokasi')
                    ->join('jamaah', 'keuangankapling.id_jamaah', '=', 'jamaah.id')
                    ->join('pivots', 'pivots.id_jamaah', '=', 'jamaah.id')
                    ->join('kapling', 'pivots.id_kapling', '=', 'kapling.id')
                    ->join('unit', 'kapling.id_unit', '=', 'unit.id')
                    ->where('keuangankapling.id', '=', $id)
                    ->find($id);
        /*dd($print);*/
        return view('Keuangan.Kapling.show', compact('keuangankapling', 'print'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $alljamaah = Penanggung::select('jamaah.id as id', 'jamaah.namajamaah as NamaJamaah', 'penanggung.id as idp', 'penanggung.namalengkap as NamaLengkap')
                    ->join('jamaah', 'penanggung.id_jamaah', '=', 'jamaah.id')
                    ->get();
        $keuangankapling = KeuanganKapling::find($id);
        return view('Keuangan.Kapling.edit', compact('alljamaah', 'keuangankapling'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $keuangankaplingupdate = Request::all();
        $keuangankapling = KeuanganKapling::find($id);
        $keuangankapling->update($keuangankaplingupdate);
        return redirect('keuangankapling');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        KeuanganKapling::find($id)->delete();
        return redirect('keuangankapling');
    }
}