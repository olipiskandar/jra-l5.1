<?php

namespace App\Http\Controllers\backend;

use DB;
use App\Http\Controllers\Controller;
use App\Model\Pivot;
use App\Model\Jamaah;
use App\Model\Kapling;
use App\Model\Unit;
use \Validator, \Redirect, \Request, \Input;

class PivotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $penempat = Pivot::whereHas('Jamaah', function($q){
            $q->where('Status', '=', 'Meninggal');
        })->get();
        /*dd($penempat);*/
        $jamaah = Jamaah::select('jamaah.*')
                    ->leftjoin('pivots', 'pivots.id_jamaah', '=', 'jamaah.id')
                    ->whereNull('pivots.id_jamaah')
                    ->where('jamaah.Status', '=', 'Meninggal')
                    /*->orderBy('jamaah.NamaJamaah', 'desc')*/
                    ->get();

        $kapling = Kapling::select('kapling.*')
                    ->leftjoin('pivots', 'pivots.id_kapling', '=', 'kapling.id')
                    ->leftjoin('bookingkapling', 'bookingkapling.id_kapling', '=', 'kapling.id')
                    ->whereNull('pivots.id_kapling')
                    ->whereNull('bookingkapling.id_kapling')
                    ->get();
        return view('Makam.Penempatan.index', compact('penempat', 'jamaah', 'kapling'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $pivot = Request::all();
        Pivot::create($pivot);
        return redirect('penempatan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $penempat = Pivot::find($id);
        $jamaah = Jamaah::select('jamaah.*')
                    ->leftjoin('pivots', 'pivots.id_jamaah', '=', 'jamaah.id')
                    ->whereNull('pivots.id_jamaah')
                    ->where('jamaah.Status', '=', 'Meninggal')
                    /*->orderBy('jamaah.NamaJamaah', 'desc')*/
                    ->get();

        $kapling = Kapling::select('kapling.*')
                    ->leftjoin('pivots', 'pivots.id_kapling', '=', 'kapling.id')
                    ->leftjoin('bookingkapling', 'bookingkapling.id_kapling', '=', 'kapling.id')
                    ->whereNull('pivots.id_kapling')
                    ->whereNull('bookingkapling.id_kapling')
                    ->get();
        return view('Makam.Penempatan.edit', compact('penempat', 'jamaah', 'kapling'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //
        $pivotupdate = Request::all();
        $pivot = Pivot::find($id);
        $pivot->update($pivotupdate);
        return redirect('penempatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Pivot::find($id)->delete();
        return Redirect::back();
    }
}
