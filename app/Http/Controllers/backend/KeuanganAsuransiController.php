<?php

namespace App\Http\Controllers\backend;

use DB;
use App\Http\Controllers\Controller;
use App\Model\Jamaah;
use App\Model\KeuanganAsuransi;
use \Validator, \Redirect, \Request, \Input;

class KeuanganAsuransiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // index
        $keuangan = KeuanganAsuransi::select('jamaah.id as idj', 'jamaah.namajamaah as NamaJamaah', 'jamaah.no_jra as no_jra','keuanganasuransi.*')
                    ->join('jamaah', 'keuanganasuransi.id_jamaah', '=', 'jamaah.id')
                    ->where('jamaah.Status', '=', 'Aktif')
                    ->orderBy('id', 'desc')
                    ->get();
        
        // Tambah Transasksi
        $jamaah = Jamaah::where('Status', '=', 'Aktif')->get();
        /*dd($keuangan);*/
        return view('Keuangan.Asuransi.index', compact('keuangan', 'jamaah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $keuangan=Request::all();
        $kirim = KeuanganAsuransi::create($keuangan);
        $insertedId = $kirim->id;
        return redirect('keuanganasuransi/'.$insertedId);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $keuanganasuransi = KeuanganAsuransi::find($id);
        return view('Keuangan.Asuransi.show', compact('keuanganasuransi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $jamaah = Jamaah::where('Status', '=', 'Aktif')->get();
        $keuangan = KeuanganAsuransi::find($id);
        return view('Keuangan.Asuransi.edit', compact('keuangan', 'jamaah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $keuanganasuransiupdate = Request::all();
        $keuanganasuransi = KeuanganAsuransi::find($id);
        $keuanganasuransi->update($keuanganasuransiupdate);
        return redirect('keuanganasuransi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        KeuanganAsuransi::find($id)->delete();
        return redirect('keuanganasuransi');
    }
}
