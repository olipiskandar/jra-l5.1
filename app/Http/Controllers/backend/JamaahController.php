<?php

namespace App\Http\Controllers\backend;

use DB;
use App\Http\Controllers\Controller;
use App\Model\Jamaah;
use App\Model\KeuanganKapling;
use App\Model\KeuanganAsuransi;
use \Validator, \Redirect, \Request, \Input;

class JamaahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dasboard()
    {
        # code...
        return view ('welcome');
    }
    
    public function index()
    {
        //
        $jamaah = Jamaah::where('Status', '=', 'Aktif')->orderBy('id', 'desc')->get();
        /*dd($jamaah);*/
        $meninggal = Jamaah::select('jamaah.*', 'kapling.kodekapling', 'unit.lokasi')
                    ->leftjoin('pivots', 'pivots.id_jamaah', '=', 'jamaah.id')
                    ->leftjoin('kapling', 'pivots.id_kapling', '=', 'kapling.id')
                    ->leftjoin('unit', 'kapling.id_unit', '=', 'unit.id')
                    ->where('jamaah.status', '=', 'Meninggal')->orderBy('jamaah.id', 'asc')->get();
        
        return view('Jamaah.index', compact('jamaah', 'meninggal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $jamaah=Request::all();
        Jamaah::create($jamaah);
        return redirect('jamaah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $jamaah = Jamaah::select('jamaah.*', 'kapling.*', 'unit.*')
                    ->leftjoin('pivots', 'pivots.id_jamaah', '=', 'jamaah.id')
                    ->leftjoin('kapling', 'pivots.id_kapling', '=', 'kapling.id')
                    ->leftjoin('unit', 'kapling.id_unit', '=', 'unit.id')
                    ->find($id);
        $penanggung = Jamaah::select('penanggung.*')
                    ->leftjoin('penanggung', 'penanggung.id_jamaah', '=', 'jamaah.id')
                    ->where('penanggung.id_jamaah', '=', $id)
                    ->get();
        $booking = Jamaah::select('jamaah.status as Status', 'unit.lokasi as Lokasi', 'kapling.kodekapling as KodeKapling')
                    ->join('bookingkapling', 'bookingkapling.id_jamaah', '=', 'jamaah.id')
                    ->join('kapling', 'bookingkapling.id_kapling', '=', 'kapling.id')
                    ->join('unit', 'kapling.id_unit', '=', 'unit.id')
                    ->find($id);
        $kapling = KeuanganKapling::where('id_jamaah', $id)->get();
        $asuransi = KeuanganAsuransi::where('id_jamaah', $id)->get();
        /*dd($kapling);*/
        return view('Jamaah.show', compact('jamaah', 'penanggung', 'booking', 'kapling', 'asuransi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $jamaah=Jamaah::find($id);
        return view('Jamaah.edit', compact('jamaah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $jamaahupdate = Request::all();
        $jamaah = Jamaah::find($id);
        $jamaah->update($jamaahupdate);
        return redirect('jamaah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        Jamaah::find($id)->delete();
        return redirect('jamaah');
    }
}