<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pivot extends Model
{
    //
    protected  $table = 'pivots';
    public $timestamps = true;
	protected $fillable = array('id_jamaah', 'id_kapling');
	protected $visible = array('id_jamaah', 'id_kapling');

	public function Jamaah(){
		return $this->belongsTo('App\Model\Jamaah', 'id_jamaah');
	}

	public function Kapling(){
		return $this->belongsTo('App\Model\Kapling', 'id_kapling');
	}
}
