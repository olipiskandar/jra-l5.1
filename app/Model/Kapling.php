<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kapling extends Model
{
    //
    protected  $table = 'kapling';
    public $timestamps = true;
	protected $fillable = array('id_unit', 'KodeKapling');
	protected $visible = array('id_unit', 'KodeKapling');

	public function Unit(){
		return $this->belongsTo('App\Model\Unit', 'id_unit');
	}

	public function BookingKapling(){
		return $this->hasMany('App\Model\BookingKapling', 'id_kapling');
	}

	public function Pivot(){
		return $this->hasMany('App\Model\Pivot', 'id_kapling');
	}
}
