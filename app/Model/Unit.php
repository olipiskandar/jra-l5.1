<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //
    protected  $table = 'unit';
    public $timestamps = true;
	protected $fillable = array('Lokasi', 'KapasitasMakam');
	protected $visible = array('Lokasi', 'KapasitasMakam');

	public function Kapling(){
		return $this->hasMany('App\Model\Kapling', 'id_unit');
	}
}
