<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Penanggung extends Model
{
    //
    protected $table = 'penanggung';
    public $timestamps = true;
	protected $fillable = array('id_jamaah', 'NamaLengkap', 'Alamat', 'Telepon', 'Telepon2');
	protected $visible = array('id_jamaah', 'NamaLengkap', 'Alamat', 'Telepon', 'Telepon2');

	public function Jamaah(){
		return $this->belongsTo('App\Model\Jamaah', 'id_jamaah');
	}

	public function KeuanganKapling()
	{
		# code...
		return $this->HasMany('App\Model\KeuanganKapling', 'id_penanggungjawab');
	}
}