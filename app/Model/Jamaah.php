<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Jamaah extends Model
{
    //
    protected $table = 'jamaah';
    public $timestamps = true;
	protected $fillable = array('TanggalDaftar', 'TanggalMeninggal', 'no_jra','NamaJamaah', 'AlamatLengkap', 'Telepon', 'Pendaftaran','Status');
	protected $visible = array('TanggalDaftar', 'TanggalMeninggal', 'no_jra','NamaJamaah', 'AlamatLengkap', 'Telepon', 'Pendaftaran','Status');

	public function BookingKapling(){
		return $this->hasMany('App\Model\BookingKapling', 'id_jamaah');
	}

	public function Penanggung(){
		return $this->hasMany('App\Model\Penanggung', 'id_jamaah');
	}

	public function KeuanganAsuransi(){
		return $this->hasMany('App\Model\KeuanganAsuransi', 'id_jamaah');
	}

	public function KeuanganKapling(){
		return $this->hasMany('App\Model\KeuanganKapling', 'id_jamaah');
	}

	public function Pivot(){
		return $this->hasMany('App\Model\Pivot', 'id_jamaah');
	}
}