<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BookingKapling extends Model
{
    //
    protected  $table = 'bookingkapling';
    public $timestamps = true;
	protected $fillable = array('id_kapling', 'id_jamaah');
	protected $visible = array('id_kapling', 'id_jamaah');

	public function Kapling(){
		return $this->belongsTo('App\Model\Kapling', 'id_kapling');
	}	

	public function Jamaah(){
		return $this->belongsTo('App\Model\Jamaah', 'id_jamaah');
	}
}
