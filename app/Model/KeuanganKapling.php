<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KeuanganKapling extends Model
{
    //
    protected  $table = 'keuangankapling';
    public $timestamps = true;
	protected $fillable = array('id_jamaah', 'id_penanggungjawab', 'Tanggal', 'JumlahUang', 'DurasiPembayaran', 'Keterangan', 'Penerima');
	protected $visible = array('id_jamaah', 'id_penanggungjawab', 'Tanggal', 'JumlahUang', 'DurasiPembayaran', 'Keterangan', 'Penerima');

	public function Jamaah()
	{
		return $this->belongsTo('App\Model\Jamaah', 'id_jamaah');
	}

	public function Penanggung()
	{
		return $this->belongsTo('App\Model\Penanggung', 'id_penanggungjawab');
	}
}