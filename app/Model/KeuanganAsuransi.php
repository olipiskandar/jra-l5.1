<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KeuanganAsuransi extends Model
{
    //
    protected  $table = 'keuanganasuransi';
    public $timestamps = true;
	protected $fillable = array('id_jamaah', 'Tanggal', 'JumlahUang', 'DurasiPembayaran', 'Keterangan', 'Penerima');
	protected $visible = array('id_jamaah', 'Tanggal', 'JumlahUang', 'DurasiPembayaran', 'Keterangan', 'Penerima');

	public function Jamaah()
	{
		return $this->belongsTo('App\Model\Jamaah', 'id_jamaah');
	}
}