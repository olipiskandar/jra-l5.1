@extends('layouts.master')
@section('title') Data @stop
@section('subtitle') Lokasi Makam @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Unit
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<ul class="nav nav-pills">
			<li class="active">
				<a href="#tab_2_1" data-toggle="tab">
				Data Makam</a>
			</li>
			<li>
				<a href="#tab_2_2" data-toggle="tab">
				Tambah Lokasi Makam</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="tab_2_1">
				<div class="data-table"></div>
				<table class="table table-striped table-bordered table-hover" id="aktif">
					<thead>
					<?php
						$no = 1;
					?>
					<tr>
						<th>#</th>
						<th>Lokasi</th>
						<th>Kapasitas Makam</th>
						<th>Kapling Kosong</th>
						<th>Aksi</th>
					</tr>
					</thead>
					@foreach ($unit as $data)
					<tbody>
							<td>{{ $no++ }}</td>
							<td>{{ $data->Lokasi }}</td>
							<td>{{ $data->KapasitasMakam }} Unit</td>
							<td> Unit</td>
							<td>
								<a href="{{route('unit.show',$data->id)}}" class="btn btn-primary">Lihat Rincian</a>
								<a href="{{route('unit.edit',$data->id)}}" class="btn btn-warning">Edit Lokasi</a>
								<a href="{{route('destroyunit',$data->id)}}" class="btn btn-danger">Hapus</a>
							</td>
					</tbody>
					@endforeach
				</table>
			</div>
			<div class="tab-pane fade" id="tab_2_2">
				<div class="portlet-body form">
					{!! Form::open(array('route' => 'unit.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Lokasi Makam</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="Lokasi">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Kapasitas Makam</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="KapasitasMakam">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#aktif').DataTable({
			// optional
			"pagingType": "full_numbers",
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });

	/*Validation*/
	
    </script>