@extends('layouts.master')
@section('title') Data @stop
@section('subtitle') Lokasi Makam {{ $unit->Lokasi }} @stop
@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="portlet box red-sunglo">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-child "></i>Informasi Unit Makam
				</div>
			</div>
			<div class="portlet-body">
				<ul>
					<li>
						 Nama Lokasi : <b> {!! $unit->Lokasi !!} </b>
					</li>
					<li>
						 Kapasitas Makam : {!! $unit->KapasitasMakam !!}
					</li>
				</ul>
			</div>
		</div>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i>Kapling Yang Belum Terdata
				</div>
			</div>
			<div class="portlet-body">
				<h3>Sisa: {!! $unit->KapasitasMakam - $sisa !!} </h3>
				{!! $persen = ($sisa / $unit->KapasitasMakam) * 100 !!}% Complete
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{!! $unit->KapasitasMakam - $sisa !!}" aria-valuemin="0" aria-valuemax="$unit->KapasitasMakam" style="width: {!! $persen !!}%">
						<span class="sr-only">{!! $persen !!}% Complete (success)</span>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box blue">
			<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Tambah Kapling
			</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				@if (($unit->KapasitasMakam - $sisa)>0)
				{!! Form::open(array('route' => 'kapling.store', 'class' => 'form-horizontal form-row-seperated')) !!}
					<meta name="csrf-token" content="{{ csrf_token() }}" />
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Lokasi Makam</label>
							<div class="col-md-9">
								<select class="bs-select form-control" data-show-subtext="true" name='id_unit' readonly>
		        					<option value="{!! $unit->id !!}">{!! $unit->Lokasi !!}
		        					</option>
		        				</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Kode</label>
							<div class="col-md-9">
								{!! Form::text('KodeKapling', Input::old('KodeKapling'), array('class' => 'form-control' , 'placeholder' => 'Contoh : 100')) !!}
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn green"><i class="fa fa-pencil"></i> Tambah</button>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
				@else
				<br><H2><center>Sudah Terdata Semua</center></H2><br>
				@endif
				
				<!-- END FORM-->
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="portlet box blue tabbable">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Detail Unit Makam
				</div>
			</div>
			<div class="portlet-body">
				<div class="data-table"></div>
					<table class="table table-striped table-hover" id="kapling">
						<thead>
						<tr>
							<th>Kode Kapling</th>
							<th>Nama Jamaah Meninggal </th>
						</tr>
						</thead>
						<tbody>
							@foreach($kapling as $data)
								<tr>
									<td>{!! $data->KodeKapling !!}
										<a href="{!! route('kapling.edit', $data->idk) !!}" class="btn btn-xs yellow">Update</i></a>
									</td>
									<td>@if($data->nama != '')
											{!! $data->nama !!}
											<a href="{!! route('jamaah.show', $data->idjamaah) !!}" class="btn btn-primary">Rincian</i></a>
											<a href="{!! route('jamaah.edit', $data->idjamaah) !!}" class="btn btn-warning">Update</i></a>
										@else
											Kosong
										@endif()
									</td>
									<!-- <td>
										<a href="{!! route('kapling.edit', $data->idk) !!}" class="btn btn-xs yellow">
											Edit <i class="fa fa-edit"></i>
										</a>
										<a href="{!! route('destroykapling', $data->idk) !!}" class="btn btn-xs red">
											Hapus <i class="fa fa-times"></i>
										</a>
									</td> -->
								</tr>
							@endforeach
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#kapling').DataTable({
			// optional
			"pagingType": "full_numbers",
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });

	/*Validation*/
	
    </script>