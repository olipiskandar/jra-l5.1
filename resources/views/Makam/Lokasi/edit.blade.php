@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Lokasi Makam @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Edit Lokasi Makam : {{ $unit->Lokasi }}
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="portlet-body form">
			{!! Form::model($unit,['method' => 'PATCH','route'=>['unit.update',$unit->id]]) !!}
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Lokasi Makam</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="Lokasi" value="{{ $unit->Lokasi }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Kapasitas Makam</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="KapasitasMakam" value="{{ $unit->KapasitasMakam }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Edit</button>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop