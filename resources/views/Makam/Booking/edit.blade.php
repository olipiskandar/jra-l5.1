@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Jamaah Yang Akan Menempati Kapling Makam @stop
@section('content')

<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Edit Booking Penempat Kapling: {{ $booking->Jamaah->NamaJamaah }} || Lokasi & Kode Kapling: {{ $booking->kapling->unit->Lokasi }} - {{ $booking->kapling->KodeKapling }}
		</div>
	</div>
	<div class="portlet-body">
		<div class="portlet-body form">
			{!! Form::model($booking,['method' => 'PATCH','route'=>['bookingkapling.update',$booking->id]]) !!}
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<div class="form-group form-md-line-input">
					<label class="col-md-2 control-label">Nama Jamaah</label>
					<div class="col-md-4">
						<select class="form-control select2me" name="id_jamaah">
							@foreach ($jamaah as $data)
							<option value="{{ $data->id }}">{{ $data->no_jra }} - {{ $data->NamaJamaah }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group form-md-line-input">
					<label class="col-md-2 control-label">Lokasi & Kode Kapling</label>
					<div class="col-md-4">
						<select class="form-control select2me" name="id_kapling">
							@foreach ($kapling as $data)
							<option value="{{ $data->id }}">{{ $data->Unit->Lokasi }} - {{ $data->KodeKapling }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Edit</button>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#aktif').DataTable({
			// optional
			"pagingType": "full_numbers",
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });
    </script>