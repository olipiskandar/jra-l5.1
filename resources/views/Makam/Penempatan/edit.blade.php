@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Jamaah Yang Menempati Makam @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Edit Nama Penempat Makam : {{ $penempat->Jamaah->NamaJamaah }} || Lokasi : {{ $penempat->Kapling->Unit->Lokasi }} || Kode Kapling{{ $penempat->Kapling->KodeKapling }}
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body form">
		{!! Form::model($penempat,['method' => 'PATCH','route'=>['penempatan.update',$penempat->id]]) !!}
			<meta name="csrf-token" content="{{ csrf_token() }}" />
			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label">Nama Jamaah</label>
				<div class="col-md-4">
					<select class="form-control select2me" name="id_jamaah">
						@foreach ($jamaah as $data)
							<option value="{{ $data->id }}">{{ $data->NamaJamaah }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group form-md-line-input">
				<label class="col-md-2 control-label">Lokasi & Kode Kapling</label>
				<div class="col-md-4">
					<select class="form-control select2me" name="id_kapling">
						@foreach ($kapling as $data)
							<option value="{{ $data->id }}">{{ $data->Unit->Lokasi }} - {{ $data->KodeKapling }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-2 col-md-10">
						<button type="submit" class="btn blue">Edit</button>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
@stop