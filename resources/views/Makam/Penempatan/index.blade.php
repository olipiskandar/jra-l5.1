@extends('layouts.master')
@section('title') Data @stop
@section('subtitle') Jamaah Yang Menempati Makam @stop
@section('content')

<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Informasi
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<ul class="nav nav-pills">
			<li class="active">
				<a href="#tab_2_1" data-toggle="tab">
				Data Jamaah</a>
			</li>
			<li>
				<a href="#tab_2_2" data-toggle="tab">
				Tambah Jamaah Yang Menempati Makam</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="tab_2_1">
				<div class="data-table"></div>
				<table class="table table-striped table-bordered table-hover" id="aktif">
					<thead>
					<tr>
						<th>#</th>
						<th>Nama Jamaah</th>
						<th>Lokasi Makam</th>
						<th>Kode Kapling</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
						<?php
							$no = 1;
						?>
						<tr>
						@foreach ($penempat as $data)
							<td>{{ $no++ }} </td>
							<td>{{ $data->Jamaah->NamaJamaah }} </td>
							<td>{{ $data->Kapling->Unit->Lokasi }} </td>
							<td>{{ $data->Kapling->KodeKapling }} </td>
							<td>
								<a href="{{route('jamaah.show',$data->Jamaah->id)}}" class="btn btn-primary">Rician</a>
								<!-- <a href="{{route('penempatan.edit',$data->id)}}" class="btn btn-warning">Update</a> -->
								<a href="{{route('destroypenempatan',$data->id)}}" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab_2_2">
				<div class="portlet-body form">
					{!! Form::open(array('route' => 'penempatan.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Nama Jamaah</label>
							<div class="col-md-4">
								<select class="form-control select2me" name="id_jamaah">
									@foreach ($jamaah as $data)
										<option value="{{ $data->id }}">{{ $data->NamaJamaah }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Lokasi & Kode Kapling</label>
							<div class="col-md-4">
								<select class="form-control select2me" name="id_kapling">
									@foreach ($kapling as $data)
										<option value="{{ $data->id }}">{{ $data->Unit->Lokasi }} - {{ $data->KodeKapling }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#aktif').DataTable({
			// optional
			"pagingType": "full_numbers",
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });
    </script>