@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Lokasi Kalpling @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Edit Lokasi Kapling : {{ $kapling->KodeKapling }}
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="portlet-body form">
			{!! Form::model($kapling,['method' => 'PATCH','route'=>['kapling.update',$kapling->id]]) !!}
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Lokasi Makam</label>
						<div class="col-md-10">
							<select class="form-control select2me" name="id_unit">
								@foreach($unit as $data)
									@if($data->Lokasi != '')
										<option value="{{ $data->id }}">{{ $data->Lokasi }}</option>
									@endif()
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Kode Kapling</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="KodeKapling" value="{{ $kapling->KodeKapling }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Edit</button>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop