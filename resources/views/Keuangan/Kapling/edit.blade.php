@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Transaksi Kapling Makam @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Data Transaksi : {{ $keuangankapling->Jamaah->NamaJamaah }} || Penanggung Jawab : {{ $keuangankapling->Penanggung->NamaLengkap }}
		</div>
	</div>
	<div class="portlet-body">
		<div class="portlet-body form">
			{!! Form::model($keuangankapling,['method' => 'PATCH','route'=>['keuangankapling.update',$keuangankapling->id]]) !!}
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<div class="form-body">
					<div class="form-group form-md-line-input" id="datePicker">
						<label class="col-md-2 control-label">Tanggal Transaksi</label>
						<div class="col-md-3">
							<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
								<input class="form-control" type="text" name="Tanggal" readonly value="{{ $keuangankapling->Tanggal }}" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nama Jamaah</label>
						<div class="col-md-4">
							<select class="form-control " name="id_jamaah" id="id_jamaah">
								@foreach($alljamaah as $data)
									<option value="{{ $data->id }}">{{ $data->NamaJamaah }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nama Penanggung</label>
						<div class="col-md-4">
							<select class="form-control " name="id_penanggungjawab" id="id_penanggungjawab">
								@foreach($alljamaah as $data)
									<option value="{{ $data->idp }}" class="{{ $data->id }}">{{ $data->NamaLengkap }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Jumlah Uang</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="JumlahUang" value="{{ $keuangankapling->JumlahUang }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Durasi Pembayaran</label>
						<div class="col-md-4">
							<select class="form-control select2me" name="DurasiPembayaran">
								<option value="6">6 Bulan</option>
								<option value="12">12 Bulan</option>
								<option value="18">18 Bulan</option>
								<option value="24">24 Bulan</option>
								<option value="30">30 Bulan</option>
								<option value="36">36 Bulan</option>
								<option value="42">42 Bulan</option>
								<option value="48">48 Bulan</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Keterangan</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="Keterangan" value="{{ $keuangankapling->Keterangan }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Asal Petugas Penerima</label>
						<div class="col-md-4">
							<input type="text" class="form-control" value="{{ $keuangankapling->Penerima }}" readonly>
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Petugas Penerima Baru</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="Penerima" value="{{ auth()->user()->name }}" readonly>
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Submit</button>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
	<script src="{{ asset('assets/global/plugins/jquery-chained/jquery.chained.min.js') }}"></script>
    <script>
    	$("#id_penanggungjawab").chained("#id_jamaah");
    </script>
@stop