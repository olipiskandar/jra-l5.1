@extends('layouts.master')
@section('title') Data @stop
@section('subtitle') Transaksi Kapling Makam @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Data Transaksi
		</div>
	</div>
	<div class="portlet-body">
		<ul class="nav nav-pills">
			<li class="active">
				<a href="#tab_2_1" data-toggle="tab">
				Data Iuran Makam</a>
			</li>
			<li>
				<a href="#tab_2_2" data-toggle="tab">
				Tambah Iuran Makam</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="tab_2_1">
				<div class="data-table"></div>
				<table class="table table-striped table-bordered table-hover" id="aktif">
					<thead>
					<tr>
						<th>#</th>
						<th>Tanggal Transaksi</th>
						<th>Nama Jamaah</th>
						<th>Jumlah Uang</th>
						<th>Durasi Pembayaran</th>
						<th>Dibayar Oleh</th>
						<th>Kode Kapling</th>
						<th>Aksi</th>
					</tr>
					</thead>
					
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($keuangan as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{!! date("d-m-Y", strtotime($data->Tanggal)) !!}</td>
							<td>{{ $data->NamaJamaah }}</td>
							<td>Rp. {!! (number_format($data->JumlahUang, 0 , '' , '.' ) . ',-') !!}</td>
							<td>{{ $data->DurasiPembayaran }} Bulan</td>
							<td>{{ $data->NamaLengkap }}</td>
							<td>{{ $data->KodeKapling }}</td>
							<td>
								<a href="{{route('jamaah.show',$data->idj)}}" class="btn btn-primary">Rincian</a>
								<a href="{{route('keuangankapling.show',$data->id)}}" class="btn btn-primary">Print</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab_2_2">
				<div class="portlet-body form">
					{!! Form::open(array('route' => 'keuangankapling.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<div class="form-body">
							<div class="form-group form-md-line-input" id="datePicker">
								<label class="col-md-2 control-label">Tanggal Transaksi</label>
								<div class="col-md-3">
									<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
										<input class="form-control" type="text" name="Tanggal" readonly/>
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nama Jamaah</label>
								<div class="col-md-4">
									<select class="form-control " name="id_jamaah" id="id_jamaah">
										@foreach($alljamaah as $data)
											<option value="{{ $data->id }}">{{ $data->NamaJamaah }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nama Penanggung</label>
								<div class="col-md-4">
									<select class="form-control " name="id_penanggungjawab" id="id_penanggungjawab">
										@foreach($alljamaah as $data)
											<option value="{{ $data->idp }}" class="{{ $data->id }}">{{ $data->NamaLengkap }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Jumlah Uang</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="JumlahUang">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Durasi Pembayaran</label>
								<div class="col-md-4">
									<select class="form-control select2me" name="DurasiPembayaran">
										<option value="6">6 Bulan</option>
										<option value="12">12 Bulan</option>
										<option value="18">18 Bulan</option>
										<option value="24">24 Bulan</option>
										<option value="30">30 Bulan</option>
										<option value="36">36 Bulan</option>
										<option value="42">42 Bulan</option>
										<option value="48">48 Bulan</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Keterangan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="Keterangan">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Petugas Penerima</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="Penerima" value="{{ auth()->user()->name }}" readonly>
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn blue">Simpan & Cetak Kwitansi Pembayaran</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset('assets/global/plugins/jquery-chained/jquery.chained.min.js') }}"></script>
    <script>
    	$("#id_penanggungjawab").chained("#id_jamaah");
    </script>
</div>
@stop
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
	$(document).ready(function(){
		$('#aktif').DataTable({
			// optional
			"pagingType": "full_numbers",
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });
    </script>