@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Transaksi Keanggotaan @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Edit Transaksi Keanggotaan : {{ $keuangan->Jamaah->NamaJamaah }} || Durasi Pembayaran : {{ $keuangan->DurasiPembayaran }} Bulan
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="portlet-body form">
			{!! Form::model($keuangan,['method' => 'PATCH','route'=>['keuanganasuransi.update',$keuangan->id]]) !!}
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<div class="form-body">
					<div class="form-group form-md-line-input" id="datePicker">
						<label class="col-md-2 control-label">Tanggal Transaksi</label>
						<div class="col-md-3">
							<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
								<input class="form-control" type="text" name="Tanggal" readonly value="{{ $keuangan->Tanggal}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nama Jamaah</label>
						<div class="col-md-4">
							<select class="form-control select2me" data-placeholder="Select..." name="id_jamaah">
								@foreach ($jamaah as $data)
								<option value="{{ $data->id }}">{{ $data->no_jra }} - {{ $data->NamaJamaah }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Jumlah Uang</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="JumlahUang" value="{{ $keuangan->JumlahUang}}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Durasi Pembayaran</label>
						<div class="col-md-4">
							<select class="form-control select2me" name="DurasiPembayaran">
								<option value="1">1 Bulan</option>
								<option value="2">2 Bulan</option>
								<option value="3">3 Bulan</option>
								<option value="4">4 Bulan</option>
								<option value="5">5 Bulan</option>
								<option value="6">6 Bulan</option>
								<option value="7">7 Bulan</option>
								<option value="8">8 Bulan</option>
								<option value="9">9 Bulan</option>
								<option value="10">10 Bulan</option>
								<option value="11">11 Bulan</option>
								<option value="12">12 Bulan</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Keterangan</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="Keterangan" value="{{ $keuangan->Keterangan}}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Asal Petugas Penerima</label>
						<div class="col-md-4">
							<input type="text" class="form-control" value="{{ $keuangan->Penerima }}" readonly>
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Petugas Penerima Baru</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="Penerima" value="{{ auth()->user()->name }}" readonly>
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Submit</button>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop