@extends('layouts.master')
@section('title') Cetak @stop
@section('subtitle') Transaksi Kenggotaan @stop
@section('content')
<div class="portlet light">
    <div>
        <div class="invoice">
            <div class="row invoice-logo">
                <div class="col-xs-12 invoice-logo-space">
                    <center><img src="{{ asset('images/logfix.png') }}" class="img-responsive" alt="" style="width: 20%; height: 20%"/></center>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box green">
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-condensed table-hover">
                                    <tbody>
                                        <tr>
                                            <td>
                                                 <strong>Tanggal</strong>
                                            </td>
                                            <td>
                                                 {!! date("d-m-Y", strtotime($keuangankapling->Tanggal)) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>No. Resi </strong>
                                            </td>
                                            <td>
                                                 {{ ($keuangankapling->id) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>Kode Kapling / Block </strong>
                                            </td>
                                            <td>
                                                 {{ $print->KodeKapling }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>Nama Almarhum/ah
                                            </td>
                                            <td>
                                                 {{ $keuangankapling->Jamaah->NamaJamaah }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>Lokasi Makam </strong>
                                            </td>
                                            <td>
                                                 {{ $print->Lokasi }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>Sudah Terima Dari </strong>
                                            </td>
                                            <td>
                                                 {{ $keuangankapling->Penanggung->NamaLengkap }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Alamat </strong>
                                            </td>
                                            <td>
                                                 {{ $keuangankapling->Penanggung->Alamat }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>Jumlah </strong>
                                            </td>
                                            <td>
                                                 Rp. {!! (number_format($keuangankapling->JumlahUang, 0 , '' , '.' ) . ',-') !!} ({{ Terbilang($keuangankapling->JumlahUang) }} Rupiah)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>Untuk Pembayaran </strong>
                                            </td>
                                            <td>
                                                 {{ $keuangankapling->DurasiPembayaran }} Bulan.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <strong>Keterangan </strong>
                                            </td>
                                            <td>
                                                 {{ ($keuangankapling->Keterangan) }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    
                </div>
                <div class="col-xs-4">
                    
                </div>
                <div class="col-xs-4 invoice-block">
                    Bandung, {{ indonesian_date() }}
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <u>{{ ($keuangankapling->Penerima) }}</u> <br/>
                    Tanda Tangan & Nama Jelas <br/><br/>
                    <a class="btn btn-lg blue hidden-print margin-bottom-1" onclick="javascript:window.print();">
                    Print <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<!-- Terbilang -->
<?php
    function Terbilang($angka) {
        // pastikan kita hanya berususan dengan tipe data numeric
        $angka = (float)$angka;
         
        // array bilangan 
        // sepuluh dan sebelas merupakan special karena awalan 'se'
        $bilangan = array(
                '',
                'Satu',
                'Dua',
                'Tiga',
                'Empat',
                'Lima',
                'Enam',
                'Tujuh',
                'Delapan',
                'Sembilan',
                'Sepuluh',
                'Sebelas'
        );
         
        // pencocokan dimulai dari satuan angka terkecil
        if ($angka < 12) {
            // mapping angka ke index array $bilangan
            return $bilangan[$angka];
        } else if ($angka < 20) {
            // bilangan 'belasan'
            // misal 18 maka 18 - 10 = 8
            return $bilangan[$angka - 10] . ' Belas';
        } else if ($angka < 100) {
            // bilangan 'puluhan'
            // misal 27 maka 27 / 10 = 2.7 (integer => 2) 'dua'
            // untuk mendapatkan sisa bagi gunakan modulus
            // 27 mod 10 = 7 'tujuh'
            $hasil_bagi = (int)($angka / 10);
            $hasil_mod = $angka % 10;
            return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
        } else if ($angka < 200) {
            // bilangan 'seratusan' (itulah indonesia knp tidak satu ratus saja? :))
            // misal 151 maka 151 = 100 = 51 (hasil berupa 'puluhan')
            // daripada menulis ulang rutin kode puluhan maka gunakan
            // saja fungsi rekursif dengan memanggil fungsi terbilang(51)
            return sprintf('Seratus %s', terbilang($angka - 100));
        } else if ($angka < 1000) {
            // bilangan 'ratusan'
            // misal 467 maka 467 / 100 = 4,67 (integer => 4) 'empat'
            // sisanya 467 mod 100 = 67 (berupa puluhan jadi gunakan rekursif terbilang(67))
            $hasil_bagi = (int)($angka / 100);
            $hasil_mod = $angka % 100;
            return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
        } else if ($angka < 2000) {
            // bilangan 'seribuan'
            // misal 1250 maka 1250 - 1000 = 250 (ratusan)
            // gunakan rekursif terbilang(250)
            return trim(sprintf('Seribu %s', terbilang($angka - 1000)));
        } else if ($angka < 1000000) {
            // bilangan 'ribuan' (sampai ratusan ribu
            $hasil_bagi = (int)($angka / 1000); // karena hasilnya bisa ratusan jadi langsung digunakan rekursif
            $hasil_mod = $angka % 1000;
            return sprintf('%s Ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
        } else if ($angka < 1000000000) {
            // bilangan 'jutaan' (sampai ratusan juta)
            // 'satu puluh' => SALAH
            // 'satu ratus' => SALAH
            // 'satu juta' => BENAR 
            // @#$%^ WT*
             
            // hasil bagi bisa satuan, belasan, ratusan jadi langsung kita gunakan rekursif
            $hasil_bagi = (int)($angka / 1000000);
            $hasil_mod = $angka % 1000000;
            return trim(sprintf('%s Juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
        } else if ($angka < 1000000000000) {
            // bilangan 'milyaran'
            $hasil_bagi = (int)($angka / 1000000000);
            // karena batas maksimum integer untuk 32bit sistem adalah 2147483647
            // maka kita gunakan fmod agar dapat menghandle angka yang lebih besar
            $hasil_mod = fmod($angka, 1000000000);
            return trim(sprintf('%s Milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
        } else if ($angka < 1000000000000000) {
            // bilangan 'triliun'
            $hasil_bagi = $angka / 1000000000000;
            $hasil_mod = fmod($angka, 1000000000000);
            return trim(sprintf('%s Triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
        } else {
            return 'Wow...';
        }
    }
?>

<!-- Hari -->
<?php
    function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
        if (trim ($timestamp) == '')
        {
                $timestamp = time ();
        }
        elseif (!ctype_digit ($timestamp))
        {
            $timestamp = strtotime ($timestamp);
        }
        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace ("/S/", "", $date_format);
        $pattern = array (
            '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
            '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
            '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
            '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
            '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
            '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
            '/April/','/June/','/July/','/August/','/September/','/October/',
            '/November/','/December/',
        );
        $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
            'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
            'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
            'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
            'Oktober','November','Desember',
        );
        $date = date ($date_format, $timestamp);
        $date = preg_replace ($pattern, $replace, $date);
        $date = "{$date} {$suffix}";
        return $date;
    } 
?>