@extends('layouts.master')
@section('title') Data @stop
@section('subtitle') Transaksi Keanggotaan @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Daftar Transaksi Keanggotaan
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<ul class="nav nav-pills">
			<li class="active">
				<a href="#tab_2_1" data-toggle="tab">
				Data Transaksi Keanggotaan</a>
			</li>
			<li>
				<a href="#tab_2_2" data-toggle="tab">
				Tambah Transaksi Keanggotaan</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="tab_2_1">
				<div class="data-table"></div>
				<table class="table table-striped table-bordered table-hover" id="aktif">
					<thead>
					<tr>
						<th>#</th>
						<th>Tanggal Transaksi</th>
						<th>No. JRA</th>
						<th>Nama Jamaah</th>
						<th>Jumlah Uang</th>
						<th>Durasi Pembayaran</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					</thead>
					
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($keuangan as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{!! date("d-m-Y", strtotime($data->Tanggal)) !!}</td>
							<td>{{ $data->no_jra }}</td>
							<td>{{ $data->NamaJamaah }}</td>
							<td>Rp. {!! (number_format($data->JumlahUang, 0 , '' , '.' ) . ',-') !!}</td>
							<td>{{ $data->DurasiPembayaran }} Bulan</td>
							<td>{{ $data->Keterangan }}</td>
							<td>
								<a href="{{route('jamaah.show',$data->idj)}}" class="btn btn-primary">Rincian</a>
								<a href="{{route('keuanganasuransi.edit',$data->id)}}" class="btn btn-warning">Update</a>
								<a href="{{route('destroykeuanganasuransi',$data->id)}}" class="btn btn-danger">Hapus</a>
								<a href="{{route('keuanganasuransi.show',$data->id)}}" class="btn btn-primary">Print</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab_2_2">
				<div class="portlet-body form">
					{!! Form::open(array('route' => 'keuanganasuransi.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<div class="form-body">
							<div class="form-group form-md-line-input" id="datePicker">
								<label class="col-md-2 control-label">Tanggal Transaksi</label>
								<div class="col-md-3">
									<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
										<input class="form-control" type="text" name="Tanggal" readonly/>
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nama Jamaah</label>
								<div class="col-md-4">
									<select class="form-control select2me" data-placeholder="Select..." name="id_jamaah">
										@foreach ($jamaah as $data)
										<option value="{{ $data->id }}">{{ $data->no_jra }} - {{ $data->NamaJamaah }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Jumlah Uang</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="JumlahUang">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Durasi Pembayaran</label>
								<div class="col-md-4">
									<select class="form-control select2me" name="DurasiPembayaran">
										<option value="1">1 Bulan</option>
										<option value="2">2 Bulan</option>
										<option value="3">3 Bulan</option>
										<option value="4">4 Bulan</option>
										<option value="5">5 Bulan</option>
										<option value="6">6 Bulan</option>
										<option value="7">7 Bulan</option>
										<option value="8">8 Bulan</option>
										<option value="9">9 Bulan</option>
										<option value="10">10 Bulan</option>
										<option value="11">11 Bulan</option>
										<option value="12">12 Bulan</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Keterangan</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="Keterangan">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Petugas Penerima</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="Penerima" value="{{ auth()->user()->name }}" readonly>
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#aktif').DataTable({
			// optional
			"pagingType": "full_numbers",
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });
    </script>