@extends('layouts.master')
@section('title') Detail @stop
@section('subtitle') Informasi @stop
@section('content')
<div class="row">
	<div class="col-md-2">
		<div class="portlet box purple-soft">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Info Jamaah
				</div>
			</div>
			<div class="portlet-body">
				<dl>
					@if ($jamaah->Status == 'Aktif')
						<dt>Status</dt>
						<dd>{{ $jamaah->Status }}</dd>
						<dt>Nama Jamaah</dt>
						<dd>{{ $jamaah->NamaJamaah }}</dd>
						<dt>Tanggal Pendaftaran</dt>
						<dd>{!! date("d-m-Y", strtotime($jamaah->TanggalDaftar)) !!}</dd>
						<dt>Nomor Registrasi JRA</dt>
						<dd>{{ $jamaah->no_jra }}</dd>
						<dt>Alamat Lengkap</dt>
						<dd>{{ $jamaah->AlamatLengkap }}</dd>
						<dt>Telepon</dt>
						<dd>{{ $jamaah->Telepon }}</dd>
						<dt>Biaya Pendaftaran</dt>
						<dd>Rp. {!! (number_format($jamaah->Pendaftaran, 0 , '' , '.' ) . ',-') !!}</dd>
						<hr>
						<a href="{{route('jamaah.edit',$jamaah->id)}}" class="btn btn-warning">Update</a>
					@elseif ($jamaah->Status == 'Meninggal')
						<dt>Status</dt>
						<dd>{{ $jamaah->Status }}</dd>
						<dt>Nama Jamaah</dt>
						<dd>{{ $jamaah->NamaJamaah }}</dd>
						<dt>Tanggal Meninggal</dt>
						<dd>{{ $jamaah->TanggalMeninggal }}</dd>
						<dt>Alamat Lengkap</dt>
						<dd>{{ $jamaah->AlamatLengkap }}</dd>
						<dt>Telepon</dt>
						<dd>{{ $jamaah->Telepon }}</dd>
						<hr>
						<a href="{{route('jamaah.edit',$jamaah->id)}}" class="btn btn-warning">Update</a>
					@endif
				</dl>
			</div>
		</div>
		@if($jamaah->Status == 'Meninggal')
			<div class="portlet box purple-soft">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>Deskripsi Makam
					</div>
				</div>
				<div class="portlet-body">
					<dl>
						<dt>Lokasi Makam</dt>
						<dd>{{ $jamaah->Lokasi }}</dd>
						<dt>Kode Kapling</dt>
						<dd>{{ $jamaah->KodeKapling }}</dd>
					</dl>
				</div>
			</div>
		@elseif($jamaah->Status == 'Aktif')
			<div class="portlet box purple-soft">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>Booking Makam
					</div>
				</div>
				<div class="portlet-body">
					<dl>
						@if($booking != '')
							<dt>Lokasi Makam</dt>
							<dd>{{ $booking->Lokasi }}</dd>
							<dt>Kode Kapling</dt>
							<dd>{{ $booking->KodeKapling }}</dd>
						@else
							<dt>Lokasi Makam</dt>
							<dd>Belum Booking</dd>
							<dt>Kode Kapling</dt>
							<dd>Belum Booking</dd>
							<hr>
							<button type="submit" class="btn blue">Booking Sekarang</button>
						@endif
					</dl>
				</div>
			</div>
		@endif
	</div>
	<div class="col-md-10">
		@if($jamaah->Status == 'Meninggal')
		<div class="portlet box purple-soft">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Penanggung Jawab
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" style="width:100%">
					<thead>
					<tr>
						<th>#</th>
						<th>Nama Penanggung Jawab</th>
						<th>Alamat Lengkap</th>
						<th>Telepon</th>
						<th>Telepon 2</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($penanggung as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{{ $data->NamaLengkap }}</td>
							<td>{{ $data->Alamat }}</td>
							<td>{{ $data->Telepon }}</td>
							<td>{{ $data->Telpon2 }}</td>
							<td>
								<a href="{{route('penanggungjawab.edit',$data->id)}}" class="btn btn-warning">Update</a>
								<a href="{{route('destroypenanggungjawab',$data->id)}}" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="portlet box purple-soft">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Tambah Transaksi
				</div>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:;" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body">
			{!! Form::open(array('route' => 'keuangankapling.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<div class="form-body">
					<div class="form-group form-md-line-input" id="datePicker">
						<label class="col-md-2 control-label">Tanggal Transaksi</label>
						<div class="col-md-3">
							<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
								<input class="form-control" type="text" name="Tanggal" readonly/>
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nama Jamaah</label>
						<div class="col-md-4">
							<select class="form-control " name="id_jamaah" id="id_jamaah">
									<option value="{{ $jamaah->id }}">{{ $jamaah->NamaJamaah }}</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nama Penanggung</label>
						<div class="col-md-4">
							<select class="form-control " name="id_penanggungjawab" id="id_penanggungjawab">
							@foreach($penanggung as $data)
									<option value="{{ $data->id }}">{{ $data->NamaLengkap }}</option>
							@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Jumlah Uang</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="JumlahUang">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Durasi Pembayaran</label>
						<div class="col-md-4">
							<select class="form-control select2me" name="DurasiPembayaran">
								<option value="6">6 Bulan</option>
								<option value="12">12 Bulan</option>
								<option value="18">18 Bulan</option>
								<option value="24">24 Bulan</option>
								<option value="30">30 Bulan</option>
								<option value="36">36 Bulan</option>
								<option value="42">42 Bulan</option>
								<option value="48">48 Bulan</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Keterangan</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="Keterangan">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Petugas Penerima</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="Penerima" value="{{ auth()->user()->name }}" readonly>
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Simpan & Cetak Kwitansi Pembayaran</button>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
			</div>
		</div>
		<div class="portlet box purple-soft">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Keuangan Sewa Makam
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="kapling" style="width:100%">
					<thead>
					<tr>
						<th>#</th>
						<th>Tanggal Transaksi</th>
						<th>Jumlah Uang</th>
						<th>Durasi Pembayaran</th>
						<th>Dibayarkan Oleh</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($kapling as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{!! date("d-m-Y", strtotime($data->Tanggal)) !!}</td>
							<td>Rp. {!! (number_format($data->JumlahUang, 0 , '' , '.' ) . ',-') !!}</td>
							<td>{{ $data->DurasiPembayaran }} Bulan</td>
							<td>{{ $data->penanggung->NamaLengkap }}</td>
							<td>
								<a href="{{route('keuangankapling.edit',$data->id)}}" class="btn btn-warning">Update</a>
								<a href="{{route('destroykeuangankapling',$data->id)}}" class="btn btn-danger">Hapus</a>
								<a href="{{route('keuangankapling.show',$data->id)}}" class="btn btn-primary">Print</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@elseif($jamaah->Status == 'Aktif')
		<div class="portlet box purple-soft">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Keuangan Bulanan
				</div>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:;" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="aktif">
					<thead>
					<tr>
						<th>#</th>
						<th>Tanggal Transaksi</th>
						<th>Jumlah Uang</th>
						<th>Durasi Pembayaran</th>
						<th>Aksi</th>
					</tr>
					</thead>
					
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($asuransi as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{!! date("d-m-Y", strtotime($data->Tanggal)) !!}</td>
							<td>Rp. {!! (number_format($data->JumlahUang, 0 , '' , '.' ) . ',-') !!}</td>
							<td>{{ $data->DurasiPembayaran }} Bulan</td>
							<td>
								<a href="{{route('keuanganasuransi.edit',$data->id)}}" class="btn btn-warning">Update</a>
								<a href="{{route('destroykeuanganasuransi',$data->id)}}" class="btn btn-danger">Hapus</a>
								<a href="{{route('keuanganasuransi.show',$data->id)}}" class="btn btn-primary">Print</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@endif
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#kapling').DataTable({
			// optional
			"pagingType": "full_numbers",
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });
    </script>