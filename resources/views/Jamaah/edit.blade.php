@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Jamaah Rahmatillah Assalaam @stop
@section('content')
<div class="portlet box purple">
	@if($jamaah->Status == 'Aktif')
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Jamaah JRA : {{ $jamaah->NamaJamaah }}
			</div>
		</div>
		<div class="portlet-body">
			<div class="portlet-body form">
				{!! Form::model($jamaah,['method' => 'PATCH','route'=>['jamaah.update',$jamaah->id]]) !!}
					<meta name="csrf-token" content="{{ csrf_token() }}" />
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Status Jamaah</label>
							<div class="col-md-10">
								<div class="col-md-4">
									<select class="bs-select form-control" name="Status">
										<option>Aktif</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input" id="datePicker">
							<label class="col-md-2 control-label">Tanggal Pedaftaran</label>
							<div class="col-md-3">
								<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
									<input class="form-control" type="text" name="TanggalDaftar" value="{{ $jamaah->TanggalDaftar }}" readonly/>
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Nomor JRA</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="no_jra" value="{{ $jamaah->no_jra }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Nama Lengkap</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="NamaJamaah" value="{{ $jamaah->NamaJamaah }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Alamat Lengkap</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="AlamatLengkap" value="{{ $jamaah->AlamatLengkap }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Nomor Telepon</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="Telepon" value="{{ $jamaah->Telepon }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Biaya Pendaftaran</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="Pendaftaran" value="{{ $jamaah->Pendaftaran }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-2 col-md-10">
								<button type="submit" class="btn blue">Edit</button>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	@elseif($jamaah->Status == 'Meninggal')
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Jamaah JRA : {{ $jamaah->NamaJamaah }}
			</div>
		</div>
		<div class="portlet-body">
			<div class="portlet-body form">
				{!! Form::model($jamaah,['method' => 'PATCH','route'=>['jamaah.update',$jamaah->id]]) !!}
					<meta name="csrf-token" content="{{ csrf_token() }}" />
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Status Jamaah</label>
							<div class="col-md-10">
								<div class="col-md-4">
									<select class="bs-select form-control" name="Status">
										<option>Meninggal</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input" id="datePicker">
							<label class="col-md-2 control-label">Tanggal Meninggal</label>
							<div class="col-md-10">
								<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
									<input class="form-control" type="text" name="TanggalMeninggal" readonly/>
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Nama Lengkap</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="NamaJamaah" value="{{ $jamaah->NamaJamaah }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Alamat Lengkap</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="AlamatLengkap" value="{{ $jamaah->AlamatLengkap }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group form-md-line-input">
							<label class="col-md-2 control-label">Nomor Telepon</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="Telepon" value="{{ $jamaah->Telepon }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-2 col-md-10">
								<button type="submit" class="btn blue">Edit</button>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	@endif
</div>
@stop