@extends('layouts.master')
@section('title') Data @stop
@section('subtitle') Jamaah Rahmatillah Assalaam @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Jamaah JRA
		</div>
	</div>
	<div class="portlet-body">
		<ul class="nav nav-pills">
			<li class="active">
				<a href="#tab_2_1" data-toggle="tab">
				Data Jamaah Aktif</a>
			</li>
			<li>
				<a href="#tab_2_2" data-toggle="tab">
				Data Jamaah Meninggal</a>
			</li>
			<li>
				<a href="#tab_2_3" data-toggle="tab">
				Tambah Jamaah Aktif</a>
			</li>
			<li>
				<a href="#tab_2_4" data-toggle="tab">
				Tambah Jamaah Meninggal</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="tab_2_1">
				<div class="data-table"></div>
				<table class="table table-striped table-bordered table-hover" id="aktif" style="width:100%">
					<thead>
					<tr>
						<th>#</th>
						<th>Tanggal Pendaftaran</th>
						<th>Nomor Registrasi JRA</th>
						<th>Nama Lengkap</th>
						<th>Biaya Pendaftaran</th>
						<th>Aksi</th>
					</tr>
					</thead>
					
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($jamaah as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{!! date("d-m-Y", strtotime($data->TanggalDaftar)) !!}</td>
							<td>{{ $data->no_jra }}</td>
							<td>{{ $data->NamaJamaah }}</td>
							<td>Rp. {!! (number_format($data->Pendaftaran, 0 , '' , '.' ) . ',-') !!}</td>
							<td>
								<a href="{{route('jamaah.show',$data->id)}}" class="btn btn-primary">Rician</a>
								<a href="{{route('jamaah.edit',$data->id)}}" class="btn btn-warning">Update</a>
								<a href="{{route('destroyjamaah',$data->id)}}" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab_2_2">
				<table class="table table-striped table-bordered table-hover" id="meninggal" style="width:100%">
					<thead>
					<tr>
						<th>#</th>
						<th>Tanggal Meninggal</th>
						<th>Nama Lengkap</th>
						<th>Lokasi Makam</th>
						<th>Kode Kapling</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($meninggal as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{!! date("d-m-Y", strtotime($data->TanggalDaftar)) !!}</td>
							<td>{{ $data->NamaJamaah }}</td>
							<td>{{ $data->Lokasi }}</td>
							<td>{{ $data->KodeKapling }}</td>
							<td>
								<a href="{{route('jamaah.show',$data->id)}}" class="btn btn-primary">Rincian</a>
								<a href="{{route('jamaah.edit',$data->id)}}" class="btn btn-warning">Update</a>
								<a href="{{route('destroyjamaah',$data->id)}}" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab_2_3">
				<div class="portlet-body form">
					{!! Form::open(array('route' => 'jamaah.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Status Jamaah</label>
								<div class="col-md-10">
									<div class="col-md-4">
										<select class="bs-select form-control" name="Status">
											<option>Aktif</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input" id="datePicker">
								<label class="col-md-2 control-label">Tanggal Pedaftaran</label>
								<div class="col-md-3">
									<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
										<input class="form-control" type="text" name="TanggalDaftar" readonly/>
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nomor JRA</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="no_jra">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nama Lengkap</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="NamaJamaah">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Alamat Lengkap</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="AlamatLengkap">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nomor Telepon</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="Telepon">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Biaya Pendaftaran</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="Pendaftaran">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			<div class="tab-pane fade" id="tab_2_4">
				<div class="portlet-body form">
					{!! Form::open(array('route' => 'jamaah.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Status Jamaah</label>
								<div class="col-md-10">
									<div class="col-md-4">
										<select class="bs-select form-control" name="Status">
											<option>Meninggal</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input" id="datePicker">
								<label class="col-md-2 control-label">Tanggal Meninggal</label>
								<div class="col-md-3">
									<div class="input-group date-picker input-daterange" data-date-format="yyyy/mm/dd">
										<input class="form-control" type="text" name="TanggalMeninggal" readonly/>
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nama Lengkap</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="NamaJamaah">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Alamat Lengkap</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="AlamatLengkap">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nomor Telepon</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="Telepon">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#aktif').DataTable({
			// optional
			"pagingType": "full_numbers",
			"autoWidth": false,
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });

    $(document).ready(function(){
		$('#meninggal').DataTable({
			// optional
			"pagingType": "full_numbers",
			"autoWidth": false,
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });
    </script>