<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
	<li class="start ">
		<a href="{{ URL::to('/dasboard') }}">
		<i class="icon-home"></i>
		<span class="title">Dashboard</span>
		</a>
	</li>
	
	{{-- Jamaah --}}
	<li>
		<a href="javascript:;">
		<i class="fa fa-slack"></i>
		<span class="title">Jamaah JRA</span>
		<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li>
				<a href="{{ URL::to('/jamaah') }}">
				<i class="fa fa-child"></i>Data Jamaah</a>
			</li>
			<li>
				<a href="{{ URL::to('/penanggungjawab') }}">
				<i class="fa fa-soundcloud"></i>Penanggung Jawab</a>
			</li>
			<li>
				<a href="{{ URL::to('/bookingkapling') }}">
				<i class="fa fa-jsfiddle"></i>Booking Kapling</a>
			</li>
		</ul>
	</li>
	
	{{-- Lokasi --}}
	<li>
		<a href="javascript:;">
		<i class="fa fa-bank"></i>
		<span class="title">Manajemen Lokasi</span>
		<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li>
				<a href="{{ URL::to('/unit') }}">
				<i class="fa fa-cubes"></i>Lokasi Makam</a>
			</li>
			<li>
				<a href="{{ URL::to('/penempatan') }}">
				<i class="fa fa-archive"></i>Penempat Makam</a>
			</li>
			<!-- <li>
				<a href="{{ URL::to('/kapling') }}">
				<i class="fa fa-empire"></i>Kapling</a>
			</li> -->
		</ul>
	</li>

	<!-- Transaksi -->
	<li>
		<a href="javascript:;">
		<i class="icon-basket"></i>
		<span class="title">Transaksi Keuangan</span>
		<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li>
				<a href="{{ URL::to('/keuanganasuransi') }}">
				<i class="icon-handbag"></i>Iuran Keanggotaan</a>
			</li>
			<li>
				<a href="{{ URL::to('/keuangankapling') }}">
				<!-- <a href="{{ URL::to('/detailtransaksi') }}"> -->
				<i class="icon-list"></i>Iuran Kapling</a>
			</li>
		</ul>
	</li>
</ul>
<!-- END SIDEBAR MENU -->