@extends('layouts.master')
@section('title') Edit @stop
@section('subtitle') Penanggung Jawab @stop
@section('content')
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Nama Lengkap : {{ $penanggung->NamaLengkap }} || Nama Jamaah : {{ $penanggung->Jamaah->NamaJamaah }}
		</div>
	</div>
	<div class="portlet-body">
		<div class="portlet-body form">
			{!! Form::model($penanggung,['method' => 'PATCH','route'=>['penanggungjawab.update',$penanggung->id]]) !!}
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Keluarga Dari</label>
						<div class="col-md-4">
							<select class="form-control select2me" name="id_jamaah">
								@foreach ($jamaah as $data)
									@if($data->no_jra != '')
										<option value="{{ $data->id }}">{{ $data->no_jra }} - {{ $data->NamaJamaah }}</option>
									@else
										<option value="{{ $data->id }}">{{ $data->NamaJamaah }}</option>
									@endif()
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nama Lengkap</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="NamaLengkap" value="{{ $penanggung->NamaLengkap }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Alamat Lengkap</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="Alamat" value="{{ $penanggung->Alamat }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nomor Telepon</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="Telepon" value="{{ $penanggung->Telepon }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group form-md-line-input">
						<label class="col-md-2 control-label">Nomor Telepon 2</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="Telepon2" value="{{ $penanggung->Telepon2 }}">
							<div class="form-control-focus">
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Edit</button>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop