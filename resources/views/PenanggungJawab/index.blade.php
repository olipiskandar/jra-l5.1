@extends('layouts.master')
@section('title') Data @stop
@section('subtitle') Penanggung Jawab Iuran Kapling Makam @stop
@section('content')

<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Daftar Penangung Jawab Iuran Kapling
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<ul class="nav nav-pills">
			<li class="active">
				<a href="#tab_2_1" data-toggle="tab">
				Data Penanggung Jawab</a>
			</li>
			<li>
				<a href="#tab_2_2" data-toggle="tab">
				Tambah Penanggung Jawab</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="tab_2_1">
				<div class="data-table"></div>
				<table class="table table-striped table-bordered table-hover" id="aktif">
					<thead>
					<tr>
						<th>#</th>
						<th>Nama Jamaah</th>
						<th>Nama Penanggung</th>
						<th>Telepon</th>
						<th>Aksi</th>
					</tr>
					</thead>
					
					<tbody>
						<?php
							$no = 1;
						?>
						@foreach ($penanggung as $data)
						<tr>
							<td>{{ $no++ }}</td>
							<td>{{ $data->NamaJamaah }}</td>
							<td>{{ $data->NamaLengkap }}</td>
							<td>{{ $data->Telepon }}</td>
							<td>
								<a href="{{route('jamaah.show',$data->idjamaah)}}" class="btn btn-primary">Rincian</a>
								<a href="{{route('penanggungjawab.edit',$data->id)}}" class="btn btn-warning">Update</a>
								<a href="{{route('destroypenanggungjawab',$data->id)}}" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab_2_2">
				<div class="portlet-body form">
					{!! Form::open(array('route' => 'penanggungjawab.store', 'class' => 'form-horizontal', 'id' => 'Tambah')) !!}
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Keluarga Dari</label>
								<div class="col-md-4">
									<select class="form-control select2me" name="id_jamaah">
										@foreach ($jamaah as $data)
											@if($data->no_jra != '')
												<option value="{{ $data->id }}">{{ $data->no_jra }} - {{ $data->NamaJamaah }}</option>
											@else
												<option value="{{ $data->id }}">{{ $data->NamaJamaah }}</option>
											@endif()
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nama Lengkap</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="NamaLengkap">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Alamat Lengkap</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="Alamat">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nomor Telepon</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="Telepon">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<label class="col-md-2 control-label">Nomor Telepon 2</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="Telepon2">
									<div class="form-control-focus">
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
	$(document).ready(function(){
		$('#aktif').DataTable({
			// optional
			"pagingType": "full_numbers",
			"autoWidth": false,
			stateSave: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		});
    });	
    </script>